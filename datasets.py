import numpy as np
from torch.utils.data import Dataset


class ESC50Data(Dataset):
  def __init__(self, base,foldNumber):
    feat = np.load("../audio-features/features/esc50-feat-uint8.npy")
    labels = np.load("../audio-features/features/esc50-labels.npy")
    labels = labels.reshape(feat.shape[0],-1).astype(np.int)
    if(base=="train"):
        self.data =  feat[labels[:,1]!=foldNumber]
        self.data = np.expand_dims(self.data, axis=1)/255.0
        self.labels = labels[labels[:,1]!=foldNumber,0]
    else:
        self.data =  feat[labels[:,1]==foldNumber]
        self.data = np.expand_dims(self.data, axis=1)/255.0
        self.labels = labels[labels[:,1]==foldNumber,0]
  def __len__(self):
    return len(self.data)
  def __getitem__(self, idx):
    return self.data[idx], self.labels[idx]

class AudioSet(Dataset):
  def __init__(self):
    feat = np.load("../audio-features/features/audioset-feat-uint8.npy")
    self.data = np.expand_dims(feat, axis=1)/255.0
    self.labels = self.data
  def __len__(self):
    return len(self.data)
  def __getitem__(self, idx):
    shift = np.random.randint(x.shape[-1],x.shape[0])
    x = self.data[idx]
    x = np.roll(x, shift, axis=-1)
    return x, self.labels[idx]