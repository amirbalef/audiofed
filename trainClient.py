import numpy as np
import torch
from tqdm import tqdm_notebook as tqdm

def train(device, model, loss_fn, train_loader, epochs, optimizer, change_lr=None,experiment=None):
  with experiment.train():
      for epoch in tqdm(range(1,epochs+1)):
        model.train()
        batch_losses=[]
        if change_lr:
          optimizer = change_lr(optimizer, epoch)
        for i, data in enumerate(train_loader):
          x, y = data
          optimizer.zero_grad()
          x = x.to(device, dtype=torch.float32)
          y = y.to(device, dtype=torch.float32)
          
          y_hat, z, mu, logvar = model(x)  # VAE

          loss = loss_fn(y_hat, x, mu, logvar)
          loss.backward()
          batch_losses.append(loss.item())
          optimizer.step()

        experiment.log_metric("loss",np.mean(batch_losses), epoch=epoch)
 
        print(f'Epoch - {epoch} Train-Loss : {np.mean(batch_losses)}')

def setlr(optimizer, lr):
  for param_group in optimizer.param_groups:
    param_group['lr'] = lr
  return optimizer

def lr_decay(optimizer, epoch):
  for param_group in optimizer.param_groups:
    learning_rate = param_group['lr']
  if epoch%100==0:
    new_lr = learning_rate / (10**(epoch//10))
    optimizer = setlr(optimizer, new_lr)
    print(f'Changed learning rate to {new_lr}')
  return optimizer
