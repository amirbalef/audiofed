from comet_ml import Experiment

import os
import argparse

import numpy as np
np.random.seed(0)

import torch
torch.manual_seed(0)

from torch.utils.data import DataLoader
import torch.optim as optim
import torch.nn as nn

from Models import resnet18, vaeResnet18
from Models import resnet  , vaeResnet

from datasets import ESC50Data, AudioSet
import trainServer, trainClient

def copyWeights(SrcModel,DscModel):
    src_dict = SrcModel.state_dict()
    dsc_dict = DscModel.state_dict()

    # 1. filter out unnecessary keys
    pretrained_dict = {}
    for k, v in dsc_dict.items():
        if k in src_dict:
            pretrained_dict[k]= src_dict[k]
        else:
            pretrained_dict[k]= v
    # 2. overwrite entries in the existing state dict
    dsc_dict.update(pretrained_dict) 
    # 3. load the new state dict
    DscModel.load_state_dict(dsc_dict)

def main(hyper_params):

    num_classes = 50
    foldNumber = 1
    z_dim = 64
    epochs = hyper_params['epochs']
    batch_size = hyper_params['batch_size']
    learning_rate = hyper_params['learning_rate']


    if(len(hyper_params['api_key'])>0):
        # Create an experiment with your api key:
        experiment = Experiment(
            api_key=hyper_params['api_key'],
            project_name="audiofed",
            workspace="amirbalef",
        )
    else:
        experiment =  Experiment(api_key="dummy", disabled=True)
    experiment.log_parameters(hyper_params)

    server_train_data =ESC50Data('train',foldNumber)
    server_valid_data =ESC50Data('valid',foldNumber)
    server_train_loader = DataLoader(server_train_data, batch_size=batch_size, shuffle=True)
    server_valid_loader = DataLoader(server_valid_data, batch_size=batch_size, shuffle=True)
    
    client_train_data =AudioSet()
    client_train_loader = DataLoader(client_train_data, batch_size=batch_size, shuffle=True)
    

    if torch.cuda.is_available():
      device=torch.device('cuda:0')
    else:
      device=torch.device('cpu')

    if hyper_params['model']== 'resnet18':
        serverModel = resnet18.Classifier(z_dim =z_dim,num_classes=num_classes)
        serverLoss_fn = nn.CrossEntropyLoss()
       
        clientModel = vaeResnet18.VAE(z_dim = z_dim)
        clientLoss_fn = vaeResnet18.loss_function
        
    if hyper_params['model']== 'resnet':
        serverModel = resnet.Classifier(z_dim =z_dim,num_classes=num_classes)
        serverLoss_fn = nn.CrossEntropyLoss()
       
        clientModel = vaeResnet.VAE(z_dim = z_dim)
        clientLoss_fn = vaeResnet.loss_function
        
    clientModel = clientModel.to(device)
    clientOptimizer = optim.Adam(clientModel.parameters(), lr=learning_rate)
        
    serverModel = serverModel.to(device)
    serverOptimizer = optim.Adam(serverModel.parameters(), lr=learning_rate)
  
    
    trainServer.train(device, serverModel, serverLoss_fn, server_train_loader, server_valid_loader, 10, serverOptimizer, trainServer.lr_decay, experiment)
    copyWeights(serverModel,clientModel)
   
    for iter in range(epochs):
        trainClient.train(device, clientModel, clientLoss_fn, client_train_loader, 1, clientOptimizer, trainClient.lr_decay, experiment)
        
        copyWeights(clientModel,serverModel)
        
        for param in serverModel.parameters():
            param.requires_grad = False
    
        serverModel.fc.weight.requires_grad = True
        serverModel.fc.bias.requires_grad = True
    
        trainServer.train(device, serverModel, serverLoss_fn, server_train_loader, server_valid_loader, 10, serverOptimizer, trainServer.lr_decay, experiment)
   
    #with open('esc50resnet.pth','wb') as f:
    #    torch.save(model, f)
    


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--api_key", type=str, default="")
    parser.add_argument("--dataset", type=str, default="ESC-50", choices=["ESC-50"])
    parser.add_argument("--model", type=str, default="resnet18", choices=["resnet18","resnet"])
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--learning_rate", type=float, default=2e-4, help="learning rate")
    parser.add_argument("--epochs", type=int, default=20)
    parser.add_argument("--localepochs", type=int, default=20)
    parser.add_argument("--optimizer", type=str, default="ADAM",choices=["ADAM"])
    args = parser.parse_args()

    print("=" * 80)
    print("Summary of training process:")
    print("Batch size: {}".format(args.batch_size))
    print("Learing rate       : {}".format(args.learning_rate))
    print("Number of epochs       : {}".format(args.epochs))
    print("Dataset       : {}".format(args.dataset))
    print("Local Model       : {}".format(args.model))
    print("=" * 80)

    hyper_params = {
        "api_key": args.api_key,
        "dataset": args.dataset,
        "model": args.model,
        "batch_size": args.batch_size,
        "learning_rate": args.learning_rate,
        "epochs": args.epochs,
        "optimizer": args.optimizer
    }
    main(hyper_params)
